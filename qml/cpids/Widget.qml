import QtQuick 2.0

Rectangle {
    property alias label: textLabel.text
    radius: 12
    Rectangle {
        width: parent.width
        height: 50
        color: Qt.darker(Qt.darker(parent.color))
        Text {
            id: textLabel
            anchors.fill: parent
            font { family: "Oregon LDO"; pointSize: 17; }
            color: "white"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }
}
