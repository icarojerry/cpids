import QtQuick 2.0
import QtQuick.XmlListModel 2.0

Row {
    anchors { horizontalCenter: parent.horizontalCenter; top: parent.top; topMargin: 58 }
    spacing: 10
    Repeater {
        id: repeater; model: 5
        Rectangle {
            clip: true
            height: 130; width: 130; radius: 4
            color: "#182225"; border.color: "white"
            Column {
                anchors { top: parent.top; topMargin: 2; left: parent.left; leftMargin: 8; right: parent.right; rightMargin: 8 }
                spacing: 3
                Text {
                    width: parent.width
                    font { pointSize: 18; } color: "white"
                    text: "LAB" + (index+1)
                    horizontalAlignment: Text.AlignHCenter
                }
                Text {
                    width: parent.width
                    font { pointSize: 8; } color: "white"
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
                Text {
                    width: parent.width
                    font { pointSize: 7; } color: "lightblue"
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
            }
        }
    }
}
