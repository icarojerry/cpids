import QtQuick 2.0
import QtQuick.XmlListModel 2.0

Item {
    anchors { top: parent.top; topMargin: 60; left: parent.left; right: parent.right; leftMargin: 10; rightMargin:10; bottom: parent.bottom; bottomMargin: 10}
    property alias feed: feed
    property alias view: view
    XmlListModel {
        id: feed
        query: "/rss/channel/item"
        XmlRole { name: "title"; query: "title/string()" }
        XmlRole { name: "pubDate"; query: "pubDate/string()" }
    }
    ListView {
        id: view
        clip: true
        model: feed
        anchors.fill: parent
        snapMode: ListView.SnapToItem
        delegate: Item {
            id: delegate
            height: column.height + 10
            width: delegate.ListView.view.width
            Column {
                y: 10
                id: column
                width: parent.width
                Text {
                    text: Qt.formatDateTime(new Date(Date.parse(model.pubDate)), "dd/MM/yyyy")
                    width: parent.width
                    wrapMode: Text.Wrap
                    font { family: "Oregon LDO"; pointSize: 9; }
                    color: "lightblue"
                    verticalAlignment: Text.AlignVCenter
                }
                Text {
                    text: model.title
                    width: parent.width
                    wrapMode: Text.Wrap
                    font { family: "Oregon LDO"; pointSize: 11; }
                    color: "white"
                    verticalAlignment: Text.AlignVCenter
                }
            }
        }
    }
}
