import QtQuick 2.0

import "sha1.js" as SHA1JS
import "oauth.js" as OAuthJS
import "twitter.js" as Twitter
import "roomschedule.js" as RoomSchedule

Rectangle {
    id: main
    width: 350; height: 400
    antialiasing: true

    property int space: 20
    property int backgroundIndex: 1

    Image {
        id: background
        anchors.fill: parent
        source: "background1.jpg"
        fillMode: Image.PreserveAspectCrop
    }

    Image {
        id: computer
        anchors { left: parent.left; leftMargin: main.space; verticalCenter: header.verticalCenter }
        source: "ifba-logo.png"
    }

    Text {
        id: header
        anchors { top: parent.top; right: parent.right; left: parent.left; topMargin: main.space/2; leftMargin: main.space; rightMargin: main.space }
        height: 100
//        color: "#52C6FF"
        color: "white"
        font { family: "Oregon LDO"; pointSize: 28 }
        text: "COORDENAÇÃO PEDAGÓGICA DE INFORMÁTICA"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    Widget {
        id: nowHappenning
        label: "Acontecendo Agora"
        color: Qt.rgba(100/256, 60/256, 193/256, 0.75)
        height: (main.height-100-(4*main.space))/3
        anchors { top: header.bottom; right: parent.right; left: parent.left; topMargin: main.space; rightMargin: 0.25*parent.width; leftMargin: main.space }
        LabView { id: nowHappeningView }
    }

    Widget {
        id: toHappen
        label: "Próximas Atividades"
        color: Qt.rgba(7/256, 146/256, 44/256, 0.75)
        height: (main.height-100-(4*main.space))/3
        anchors { top: nowHappenning.bottom; right: parent.right; left: parent.left; topMargin: main.space; rightMargin: 0.25*parent.width; leftMargin: main.space }
        LabView { id: toHappenView }
    }

    Widget {
        id: messages
        label: "Avisos dos Professores"
        color: Qt.rgba(224/256, 106/256, 32/256, 0.75)
        clip: true
        radius: 8
        height: (main.height-100-(4*main.space))/3
        anchors { top: toHappen.bottom; right: parent.right; left: parent.left; topMargin: main.space; rightMargin: 0.25*parent.width; leftMargin: main.space }
        ListModel {
            id: model
//            ListElement { text: "INF011: a prova de hoje será realizada na sala 10 do pavilhão 2"; sender: "andradesandro" }
        }
        ListView {
            clip: true
            id: view
            model: model
            anchors.fill: parent
            orientation: ListView.Horizontal
            snapMode: ListView.SnapOneItem
            highlightMoveDuration: 500
            delegate: Rectangle {
                width: view.width; height: view.height
                color: "transparent"
                Text {
                    id: message
                    width: 0.9*parent.width
                    anchors.centerIn: parent
                    text: model.text
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font { family: "Oregon LDO"; pointSize: 26 }
                    color: "#FEFEFE"
                }
                Text {
                    id: from
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    anchors { top: message.bottom; topMargin: main.space/2; horizontalCenter: parent.horizontalCenter }
                    text: "Enviado por: " + model.sender.name + " às " + Qt.formatDateTime(new Date(Date.parse(model.created_at)), "HH:mm:ss")
                    font { family: "Oregon LDO"; pointSize: 13 }
                    color: "#FEFEFE"
                }
            }
        }
    }

    Widget {
        id: ads
        label: "Notícias de ADS"
        color: Qt.rgba(176/256, 26/256, 64/256, 0.75)
        anchors { top: header.bottom; right: parent.right; left: messages.right; topMargin: main.space; rightMargin: main.space; leftMargin: main.space }
        height: (main.height-100-(3*main.space))/2
        FeedReader {
            id: adsFeedReader
            feed.source: "http://www.wiki.ifba.edu.br/ads/tiki-blogs_rss.php?ver=2"
        }
    }

    Widget {
        id: gsort
        label: "Notícias do GSORT"
        color: Qt.rgba(1/256, 135/256, 162/256, 0.75)
        anchors { top: ads.bottom; right: parent.right; left: messages.right; topMargin: main.space; rightMargin: main.space; leftMargin: main.space }
        height: (main.height-100-(3*main.space))/2
        FeedReader {
            id: gsortFeedReader
            feed.source: "http://www.wiki.ifba.edu.br/gsort/tiki-blogs_rss.php?ver=2"
        }
    }

    Timer {
        interval: 5000; running: true; repeat: true
        onTriggered: { view.currentIndex = (view.currentIndex+1)%(view.count) }
    }

    Timer {
        interval: 2000; running: true; repeat: true
        onTriggered: {
            adsFeedReader.view.currentIndex = (adsFeedReader.view.currentIndex+1)%(adsFeedReader.view.count)
            gsortFeedReader.view.currentIndex = (gsortFeedReader.view.currentIndex+1)%(gsortFeedReader.view.count)
        }
    }

    Timer {
        interval: 60000; running: true; repeat: true
        onTriggered: {
            Twitter.loadTweets()
            RoomSchedule.loadRoomSchedule()
            adsFeedReader.feed.reload()
            gsortFeedReader.feed.reload()
            backgroundIndex = backgroundIndex < 5 ? backgroundIndex+1:1
            background.source = "background" + backgroundIndex + ".jpg"
        }
    }

    Component.onCompleted: {
        Twitter.loadTweets()
        RoomSchedule.loadRoomSchedule()
    }

    property string accessToken;
}
